import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { WeatherService } from './services/weather.service';
import { WeatherData } from './models/weather.model';
import { FormsModule } from '@angular/forms';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,FormsModule, NgFor],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit{
  city='Plovdiv';
  citySearch='';
  
  constructor(private weatherService:WeatherService){}
  onSubmit(){
    this.city=this.citySearch;
    console.warn(this.citySearch);
    console.warn(this.city);
    this.ngOnInit();

  }
  weatherData?:WeatherData;

    dayName=this.weatherData?.forecasts[0].day.toString();
  
  ngOnInit(): void {
    this.weatherService.getWeather(this.city)
    .subscribe({next: (response)=>{
      this.weatherData=response;
      console.log(response)}});
      
  }
  
  
}
