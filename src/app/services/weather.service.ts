import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.development';
import { WeatherData } from '../models/weather.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http:HttpClient) {}
    getWeather(cityName: string) : Observable<WeatherData>{
      return this.http.get<WeatherData>(environment.weatherApiBaseUrl, {
        headers: new HttpHeaders()
        .set(environment.XRapidAPIHostHeaderName,environment.XRapidAPIHostValue)
        .set(environment.XRapidAPIKeyHeaderName,environment.XRapidAPIKeyValue),
        params: new HttpParams()
        .set('location',cityName)
        .set('u', 'c')
        .set('format','json')
      });
    }
}
