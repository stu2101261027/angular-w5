import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { AppComponent } from './app.component';


const routes: Routes = [{path:'',component:AppComponent}];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,NgModule,AppRoutingModule,RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule { }
